#include <docodemo.h>

/*
moodify two lines program of variant.cpp
C:\Users\****\.platformio\packages\framework-arduino-samd\variants\arduino_mzero\variant.cpp
 { PORTA,  8, PIO_TIMER,
 { PORTA,  9, PIO_TIMER,

as follow,
 { PORTA,  8, PIO_SERCOM_ALT,
 { PORTA,  9, PIO_SERCOM_ALT,
*/

BME280 exSensorDm = BME280();
DOCODEMO Dm = DOCODEMO();

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  Dm.begin();
  Dm.LedCtrl(GREEN_LED, ON);
  Dm.LedCtrl(RED_LED, OFF);

  Dm.exPowerCtrl(ON);
#ifdef MINI_V2
  Dm.exUartPowerCtrl(ON);
  Dm.exI2CPowerCtrl(ON);
#else
  Dm.exComPowerCtrl(ON);
#endif

  delay(100);//wait for I2C device initialize if necessary.

  I2C_external.begin();// defined in the docodemo.cpp
  I2C_external.setClock(100000);

  // BME280
  exSensorDm.setI2CAddress(0x76); // BME280_I2C_ADD

  if (exSensorDm.beginI2C(I2C_external) == false)// Important! library should change the I2C driver.
  {
    SerialDebug.println("Sensor connect failed");
    Dm.LedCtrl(RED_LED, ON);
  }
  else
  {
    SerialDebug.println("Sensor connected");
  }

  SerialDebug.println("start main loop");

  uint32_t elaspTime = millis();
  while (1)
  {
    uint32_t now = millis();
    if ((now - elaspTime) >= 1000)//every one second.
    {
      elaspTime = now;
      Dm.LedCtrl(GREEN_LED, ON);

      float ExTemp = exSensorDm.readTempC();
      SerialDebug.println("T: " + String(ExTemp, 1)+"℃");

      float ExHum = exSensorDm.readFloatHumidity();
      SerialDebug.println("H: " + String(ExHum, 1) + "%");

      float ExPress = exSensorDm.readFloatPressure() / 100.0;
      SerialDebug.println("P: " + String(ExPress, 0) +"hPa");

      Dm.LedCtrl(GREEN_LED, OFF);
    }
  }
}

void setup()
{
  SerialDebug.begin(115200);
  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  // no code here...
}